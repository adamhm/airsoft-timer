# Airsoft Game Timer/Objective

This project is a handheld timer designed for use in Airsoft games, either as a game timer or as an objective for players to fight over. It is powered by a standard 9V PP3 battery and features an LCD and buzzer, with many possible configurations to suit different games. A few suggestions for games using one or more of the timers are included in the manual.

The runtime of the timer with a fresh battery was tested (using a GP Ultra 1604AU) and ran for over 25 hours in total. While the runtime will not be that long in practice as a result of increased activity due to player interaction and differing timer settings/sounding alerts etc. a single battery should last long enough for at least a few regular game days (assuming around 6h of use per day).

![Assembled timer](images/timer-menu1.jpg "Assembled timer - Menu 1") ![Assembled timer](images/timer-menu2.jpg "Assembled timer - Menu 2")
![Assembled timer](images/timer-redteam.jpg "Assembled timer - Red Timer") ![Assembled timer](images/timer-greenteam.jpg "Assembled timer - Green Timer")

Description of files in this repository:

- airsoft-timer.X: MPLAB X project directory containing the firmware source and binary code
- airsoft-timer-xp9vb: Directory containing the KiCAD schematic diagram and PCB design (with Gerber files in the gerber subdirectory)
- images: Directory containing some photographs of the completed timer and stages of construction
- airsoft-timer-BOM.txt: Bill of Materials (BOM)
- airsoft-timer-manual.odt: User manual for the Airsoft Game Timer/Objective
- LICENSE.md: Licensing information for this project
- polycarbonate-insert-diagram.pdf: Diagram for the polycarbonate insert
- polycarbonate-insert-diagram.kicad_pcb: Diagram for the polycarbonate insert (source file for the PDF)
- README.md: This file


## Assembly instructions

The PCB design was created using KiCAD and the KiCAD project files can be found in the 'airsoft-timer-xp9vb' directory. The included Gerber files were generated according to the specifications for fabrication by JLCPCB.

The overall cost of parts should be somewhere around £25-30 in single/low unit quantities; see the BOM list for the required parts.

Build order:

1. Assemble the PCB and lower half of the case + end panel
2. Prepare the top cover
3. Prepare the polycarbonate insert
4. Attach polycarbonate insert and keypad to top cover
5. Plug keypad into main PCB & attach top cover assembly

(Note: the photos here are of the v1.01 PCB; this is identical to the current v1.02 design except for some small silkscreen changes)


### 1. Assemble the PCB and lower half of the case + end panel

Start by assembling the regulator section (everything in the lower half of the board up to and including C5 except for the battery contacts) and test that to make sure that it's working correctly before proceeding with everything else - connecting power to it should result in it providing about 3.3V to the rest of the circuit (you can use pins 2 & 3 of the ICSP header/footprint as test points for this).

![Assembled regulator block](images/assembly-image1.jpg "Assembled regulator block")

Once this is done install the rest of the components except for the battery contacts, buzzer and LCD module. When all of the components aside from those have been soldered to the board, install the four 10mm F-F M3 threaded standoffs/spacers into the four mounting holes in the corners of the LCD footprint using M3x6mm pan head screws.

Note:
 - Using some threadlock on the spacer screws may be a good idea to stop them shaking loose over time
 - When installing the crystal, secure it to the board with some adhesive or tape. I use some double-sided tape for this along with a small strip of kapton tape over the crystal to further secure it to the board.
 - The ICSP header (J2) can be omitted if you have a PIC18F24K42 that's been pre-programmed with the timer firmware & don't intend to reprogram it.

![Completed PCB sans buzzer and LCD](images/assembly-image2.jpg "Completed PCB sans buzzer and LCD")

Before attaching the buzzer to the PCB the end panel needs to be prepared and the buzzer attached to it. A ~3mm hole should be drilled roughly in the center of the end panel for the buzzer's aperture; once this has been done mount the buzzer to the rear of the panel using hot glue, with the buzzer's aperture centered within the hole. Trailing wires should be soldered to the buzzer and the other ends soldered directly to the BZ1 footprint pads on the main PCB.

![Completed PCB with buzzer + end panel](images/assembly-image3.jpg "Completed PCB with buzzer + end panel")

The pin header on the LCD module will need to be desoldered and the LCD module will instead need to be connected using a short length of 2.54mm pitch ribbon cable, as the pin headers + sockets will make it too tall to fit; a 17mm long piece should be sufficient (10mm spacing + 2x 3mm stripped ends + 1mm slack accounting for measurement errors and to allow for manoeuvring the board into place). Since the module's SDO pin (pin 9) is unused you can use an 8-wire cable and leave pin 9 disconnected.

At this stage all of the components should now be soldered to the PCB except for the battery contacts. For these, start by soldering a short length of wire to each one - make sure it's long enough to reach the BT1 footprint pads on the PCB in the case - then press them into their respective slots in the case's battery compartment (make sure to fit them correctly), feeding the wires through into the case, then solder the other ends to their respective pads on the PCB. Make sure that the wires will not get pinched between the mounting pillars when the two halves of the case are put together.

Finally, mount the main PCB into the case along with the end cover + buzzer using the 3x M2x5mm self-tapping washer-head screws to secure it, and then mount the LCD module on to the spacers using the remaining M3x6mm pan head screws.

![Completed lower half of case](images/assembly-image4.jpg "Completed lower half of case")

The lower case assembly is now complete. You can verify that everything is working by connecting a battery (and programming the PIC18F24K42 via ICSP if it was not pre-programmed) - the menu should be displayed if assembly was successful; if not then check that components have been correctly placed and soldered. To verify that the 32768Hz crystal oscillator is working you can temporarily connect the keypad to the board and start the timer; if the crystal oscillator is not working correctly it will fail to count or will be inaccurate. Set an option like "beep on switch" to test the buzzer as well.


### 2. Prepare the top cover

There are two cutouts that need to be made in the top cover: one for the display window, and one for the keypad cable to pass through.

The cutout for the display window should be about 46x34mm (the same size as the active area of the display panel). The top of the display window should be positioned about 20.5mm down from the top wall of the external insert area, the bottom of the display window should be about 54.5mm down from the top wall and the sides of the display window should be about 10mm from the side walls of the external insert area. The edges of the display window should be bevelled on the external face.

For the keypad cable hole simply make a 12x3mm cutout 3mm up from the bottom wall in the middle of the external insert area. Round off the edges to avoid risking damage to the keypad cable.

![Prepared top cover](images/assembly-image5.jpg "Prepared top cover")


### 3. Prepare the polycarbonate insert

Start with a 65.5x120mm piece of 2mm thick clear polycarbonate and cut out a section in the middle of the bottom edge measuring 36.1x55.1mm, as per the diagram. Bevel off all of the edges on the outside face by about 1mm to make it look neater & prevent snagging on things.

Painting is optional but strongly recommended for best results (and different colour paints could be used for differentiating between timers). Start by applying some masking tape to the area above the LCD cutout and mark out a 48x36mm box for the window; this should be positioned 19.5mm down from the top of the insert with about 9mm either side of it. Use a sharp knife to score the tape around the window, remove the excess masking tape, then use some fine grit paper to lightly roughen up the surface of the exposed polycarbonate on the top side. Next place the insert on a flat surface top side up and apply a couple of coats of spray paint. Once the paint has dried remove the remaining masking tape to reveal the window.


### 4. Attach polycarbonate insert and keypad to top cover

(Before proceeding with this step you should test fit without using any tape or peeling the backing off of the keypad to make sure everything fits as it should and that no adjustments are necessary. Both the insert and keypad should fit neatly, with minimal gaps)

Attach the insert to the case before the keypad. To do this apply some double-sided tape to the external insert area in the top cover around the LCD cutout and either side of where the keypad will fit. Once this is done, line the insert up (aligning with the top edge) and simply press it into place.

Next peel the backing off of the keypad, route its cable through the hole and then press the keypad into the recess left by the polycarbonate insert, aligning with the bottom edge. The top cover assembly is now complete and ready to be fitted to the lower case assembly.


### 5. Plug keypad into main PCB & attach top cover

Finally, to complete assembly carefully remove the protective film from the LCD, plug the keypad cable into the right-angle header underneath the LCD (J1), then fit the top cover to the bottom half of the case and secure it using the screws supplied with the case. Be careful not to pinch or bend the keypad cable at a sharp angle when doing this; it may be helpful to use a screwdriver or other tool to help guide the cable while the two halves are being fitted together.
