/*
 *	Airsoft Game Timer/Objective
 *	Copyright (C) 2021 Adamhm
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

// PIC18F24K42 Configuration Bit Settings
// CONFIG1L
#pragma config FEXTOSC = OFF			// External Oscillator Selection (Oscillator not enabled)
#pragma config RSTOSC = HFINTOSC_64MHZ	// Reset Oscillator Selection (HFINTOSC with HFFRQ = 64 MHz and CDIV = 1:1)

// CONFIG1H
#pragma config CLKOUTEN = OFF			// Clock out Enable bit (CLKOUT function is disabled)
#pragma config PR1WAY = ON				// PRLOCKED One-Way Set Enable bit (PRLOCK bit can be cleared and set only once)
#pragma config CSWEN = ON				// Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = OFF				// Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)

// CONFIG2L
#pragma config MCLRE = EXTMCLR			// MCLR Enable bit (If LVP = 0, MCLR pin is MCLR; If LVP = 1, RE3 pin function is MCLR )
#pragma config PWRTS = PWRT_16			// Power-up timer selection bits (PWRT set at 16ms)
#pragma config MVECEN = ON				// Multi-vector enable bit (Multi-vector enabled, Vector table used for interrupts)
#pragma config IVT1WAY = ON				// IVTLOCK bit One-way set enable bit (IVTLOCK bit can be cleared and set only once)
#pragma config LPBOREN = OFF			// Low Power BOR Enable bit (ULPBOR disabled)
#pragma config BOREN = ON				// Brown-out Reset Enable bits (Brown-out Reset enabled)

// CONFIG2H
#pragma config BORV = VBOR_2P45			// Brown-out Reset Voltage Selection bits (Brown-out Reset Voltage (VBOR) set to 2.45V)
#pragma config ZCD = OFF				// ZCD Disable bit (ZCD disabled. ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PPS1WAY = OFF			// PPSLOCK bit One-Way Set Enable bit (PPSLOCK bit can be cleared and set only once; PPS registers remain locked after one clear/set cycle)
#pragma config STVREN = ON				// Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config DEBUG = OFF				// Debugger Enable bit (Background debugger disabled)
#pragma config XINST = OFF				// Extended Instruction Set Enable bit (Extended Instruction Set and Indexed Addressing Mode disabled)

// CONFIG3L
#pragma config WDTCPS = WDTCPS_31		// WDT Period selection bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = OFF				// WDT operating mode (WDT Disabled; SWDTEN is ignored)

// CONFIG3H
#pragma config WDTCWS = WDTCWS_7		// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC				// WDT input clock selector (Software Control)

// CONFIG4L
#pragma config BBSIZE = BBSIZE_512		// Boot Block Size selection bits (Boot Block size is 512 words)
#pragma config BBEN = OFF				// Boot Block enable bit (Boot block disabled)
#pragma config SAFEN = OFF				// Storage Area Flash enable bit (SAF disabled)
#pragma config WRTAPP = OFF				// Application Block write protection bit (Application Block not write protected)

// CONFIG4H
#pragma config WRTB = OFF				// Configuration Register Write Protection bit (Configuration registers (300000-30000Bh) not write-protected)
#pragma config WRTC = OFF				// Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF				// Data EEPROM Write Protection bit (Data EEPROM not write-protected)
#pragma config WRTSAF = OFF				// SAF Write protection bit (SAF not Write Protected)
#pragma config LVP = OFF				// Low Voltage Programming Enable bit (HV on MCLR/VPP must be used for programming)

// CONFIG5L
#pragma config CP = OFF					// PFM and Data EEPROM Code Protection bit (PFM and Data EEPROM code protection disabled)

// CONFIG5H

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pic18f24k42.h>
#include "airsoft-timer.h"
#include "interrupts.h"
#include "ILI9341.h"

// Variables for storing timer settings
// Countdown mode flag is in TimerStatus so the Timer0 interrupt routine can access it
struct
{
	uint8_t WaitBeep;
	uint8_t WaitBeepDelay;
	uint8_t ActiveBeep;
	uint8_t HoldTime;
	uint8_t CDSwitchBehavior;
	_Bool BeepDuringSwitch : 1;
	_Bool BeepOnSwitch : 1;
	_Bool ABeepWhilePaused : 1;
} Settings = { 0, 0, 0, 0, 0, 0, 0, 0 } ;

// Timer status variables
struct TIMER_STATUS_STRUCT TimerStatus;

uint8_t red_h;
uint8_t red_m;
uint8_t red_s;
uint8_t red_cd_h;
uint8_t red_cd_m;
uint8_t red_cd_s;

uint8_t green_h;
uint8_t green_m;
uint8_t green_s;
uint8_t green_cd_h;
uint8_t green_cd_m;
uint8_t green_cd_s;

uint8_t countdown_h;
uint8_t countdown_m;
uint8_t countdown_s;

void TimerMenu(void);
void WaitRed_Repeat(void);
void WaitGreen_Repeat(void);
void DrawBorder(void);
void DrawMenu(uint8_t lineupdate);
void DrawCursor(uint8_t pos, _Bool clear);
void DisplayNum(uint8_t x, uint16_t y, uint8_t num, uint8_t size);
void RunTimer(void);
void UpdateTimerDisplay(void);
void ToggleAltDisplay(void);

void main()
{
// Button inputs on RA0-2 (Red, Green, Blue)
// Piezo transducer on RA3
// RC0/1 are SOSC pins
// LCD connections on RC4-7
// All unused I/O pins are set as outputs and driven low as per the datasheet
	LATA = 0;
	LATB = 0;
	LATC = 0;
	ANSELA = 0;
	ANSELB = 0;
	ANSELC = 0;
    TRISA = 0b00000111;
    TRISB = 0b00000000;
    TRISC = 0b00000011;
	SLRCONC = 0b00111111; // Slew rate needs to be unlimited for at least the SDO & SCK pins to operate at max SPI speed.
	WPUA = 0b00000111;
	BUZZER = 0;
// Configure SPI port
// LCD connections:
// Reset = RC4
// DC/RS = RC5
// SDO = RC6
// SCK = RC7
// SDI is unused
// The LCD's CS pin is permanently active as no other SPI device is used
	RC6PPS = 0b00011111;
	RC7PPS = 0b00011110;
	SPI1CLK = 0b00000001; // Use 64MHz HFINTOSC for SPI clock source
	SPI1BAUD = 0x00; // = 32MHz SPI clock
	SPI1CON0 = 0b00000011; // SPI master, SPI1TWIDTH setting applies to every byte
	SPI1CON1 = 0b11000100; // SDI input sampled at end of data output time, output data is changed on SCK active to idle transition, SCK idles low, SDO/SDI active high
	SPI1CON2 = 0b00000010; // SPI TX only
// Set SOSC to high power mode for faster startup & better reliability
	OSCCON3bits.SOSCPWR = 1;
// Enable secondary 32768Hz external oscillator for Timer0
	OSCENbits.SOSCEN = 1;
// Configure Timer0: 16-bit timer, 1:1 postscaler
	T0CON0 = 0b00010000;
// Use 32768Hz SOSC clock for Timer0, not synced to system, 1:1 prescaler
	T0CON1 = 0b11010000;
// Clear Timer0's counter and interrupt flag and enable the Timer0 interrupt
	TMR0H = TIMER0_RESETVAL >> 8;
	TMR0L = (uint8_t)TIMER0_RESETVAL;
	PIR3bits.TMR0IF = 0;
	PIE3bits.TMR0IE = 1;
// Use 31kHz LFINTOSC for Timer2.
	T2CLK = 0b00000100;
// Set Timer2's period, clear its interrupt flag and enable the Timer2 interrupt
	T2PR = TIMER2_PERIOD;
	PIR4bits.TMR2IF = 0;
	PIE4bits.TMR2IE = 1;
// PWM5 setup for piezo transducer/buzzer.
	RA3PPS = 0b00001101; // Assign PWM5 output to RA3.
	CCPTMRS1 = 0b01011110; // Configure PWM5 to user Timer4; PWM6 will be set to use Timer6 in case the older display with PWM backlight is used
	T4CLK = 0x01; // Set Timer4 to use Fosc/4
// T4PR Values for various transducers:
// 0xDD with 64MHz Fosc + 1:16 prescale = ~4.5kHz
// 0xF0 with 64MHz Fosc + 1:16 prescale = ~4.15kHz
// 0xB6 with 64MHz Fosc + 1:32 prescale = ~2.73kHz
// 0xCF with 64MHz Fosc + 1:32 prescale = ~2.4kHz
#ifdef BUZZER_4KHZ
	T4CON = 0b01000000;	// 1:16 prescale
	T4PR = 0xF0;	// = ~4.15kHz with 64MHz Fosc + 1:16 prescale
// Set ~50% duty cycle
	PWM5DCH = 0x82;
	PWM5DCL = 0x00;
#else
	T4CON = 0b01010000; // 1:32 prescale
	T4PR = 0xB6;	// = ~2.73kHz with 64MHz Fosc + 1:32 prescale
// Set ~50% duty cycle
	PWM5DCH = 0x5E;
	PWM5DCL = 0x00;
#endif
	T4CONbits.ON = 1;
// Enable SPI port and set up display
	SPI1CON0bits.EN = 1;
	LCD_Init();
	LCD_Blank();
#ifdef PWM_BACKLIGHT
// The v1.1 revision of the 2.2" ILI9341 display module no longer uses a transistor switch
// to control the backlight and is now just a single 3.9R resistor in series with the backlight
// LEDs. In the revised design a resistor is used to set the backlight brightness.
// If using the older module compile this with PWM_BACKLIGHT set and connect the RC3 pin directly
// to the LED pin on the module.
// PWM6 will control the backlight brightness control. PWM6 will use Timer6.

// Assign PWM6 output to RC3.
	RC3PPS = 0b00001110;
// Set timer6 period register
	T6PR = 0xFF; // 0xFF = ~31.25kHz with 64MHz Fosc (16MHz Fosc/4) + 1:2 prescale.
// Set Timer6 to use Fosc/4, 1:2 prescale
	T6CLK = 0x01;
	T6CON = 0b00010000;
// Set duty cycle for PWM6.
	PWM6DCH = 0x40;
	PWM6DCL = 0b00000000;
	T6CONbits.ON = 1;
#endif
// Enable interrupts and activate the associated timers
	INTCON0 = 0b11100000;
	T0CON0bits.EN = 1;
	T2CONbits.ON = 1;

	while (1)
		TimerMenu();
}

void TimerMenu()
{
	uint8_t menu_opt = 0;
	POINT_COLOR = WHITE;
	DrawBorder();
	DrawMenu(0xFF);
	POINT_COLOR = RED;
	DrawCursor(0,0);
#ifdef PWM_BACKLIGHT
	LCD_BACKLIGHT = 1;
#endif
	while (ButtonStates.Red || ButtonStates.Green || ButtonStates.Blue)
		;
	while (1)
	{
		while (ButtonStates.Red || ButtonStates.Green)
		{
			DrawCursor(menu_opt, 1);
			if (ButtonStates.Red)
			{
				if (--menu_opt > NUM_MENU_LINES-1)
					menu_opt = NUM_MENU_LINES-1;
			}
			if (ButtonStates.Green)
			{
				if (++menu_opt > NUM_MENU_LINES-1)
					menu_opt = 0;
			}
			DrawCursor(menu_opt, 0);
			WaitRed_Repeat();
			WaitGreen_Repeat();
		}
		if (ButtonStates.Blue)
		{
			POINT_COLOR = WHITE;
			switch (menu_opt)
			{
				case OPT_TIMER_MODE:
					while (ButtonStates.Blue && blue_holdtime < 1)
						;
					if (TimerStatus.Countdown)
					{
						if (!ButtonStates.Blue)
							TimerStatus.Countdown = 0;
					}
					else
						TimerStatus.Countdown = 1;
					if (TimerStatus.Countdown && ButtonStates.Blue)
					{
						DrawMenu(OPT_TIMER_MODE);
						POINT_COLOR = RED;
						DisplayNum(MENU_LINE_TIMER_MODE_X, 152, countdown_h, 2);
						while (ButtonStates.Blue)
							;
						while(!ButtonStates.Blue)
						{
							while (ButtonStates.Red || ButtonStates.Green)
							{
								if (ButtonStates.Red)
								{
									if (++countdown_h > 99)
										countdown_h = 0;
								}
								if (ButtonStates.Green)
								{
									if (--countdown_h > 99)
										countdown_h = 99;
								}
								DisplayNum(MENU_LINE_TIMER_MODE_X, 152, countdown_h, 2);
								WaitRed_Repeat();
								WaitGreen_Repeat();
							}
						}
						POINT_COLOR = WHITE;
						DisplayNum(MENU_LINE_TIMER_MODE_X, 152, countdown_h, 2);
						POINT_COLOR = RED;
						DisplayNum(MENU_LINE_TIMER_MODE_X, 188, countdown_m, 2);
						while (ButtonStates.Blue)
							;
						while(!ButtonStates.Blue)
						{
							while(ButtonStates.Red || ButtonStates.Green)
							{
								if (ButtonStates.Red)
								{
									if (++countdown_m > 59)
										countdown_m = 0;
								}
								if (ButtonStates.Green)
								{
									if (--countdown_m > 59)
										countdown_m = 59;
								}
								DisplayNum(MENU_LINE_TIMER_MODE_X, 188, countdown_m, 2);
								WaitRed_Repeat();
								WaitGreen_Repeat();
							}
						}
						POINT_COLOR = WHITE;
						DisplayNum(MENU_LINE_TIMER_MODE_X, 188, countdown_m, 2);
						POINT_COLOR = RED;
						DisplayNum(MENU_LINE_TIMER_MODE_X, 224, countdown_s, 2);
						while (ButtonStates.Blue)
							;
						while(!ButtonStates.Blue)
						{
							while (ButtonStates.Red || ButtonStates.Green)
							{
								if (ButtonStates.Red)
								{
									if (++countdown_s > 59)
										countdown_s = 0;
								}
								if (ButtonStates.Green)
								{
									if (--countdown_s > 59)
										countdown_s = 59;
								}
								DisplayNum(MENU_LINE_TIMER_MODE_X, 224, countdown_s, 2);
								WaitRed_Repeat();
								WaitGreen_Repeat();
							}
						}
					}
					DrawMenu(OPT_TIMER_MODE);
					break;
				case OPT_WAITBEEP:
					LCD_DrawChar(MENU_LINE_WAITBEEP_X, 176, 's', 2);
					POINT_COLOR = RED;
					DisplayNum(MENU_LINE_WAITBEEP_X, 152, Settings.WaitBeep, 2);
					while (ButtonStates.Blue)
						;
					while(!ButtonStates.Blue)
					{
						while (ButtonStates.Red || ButtonStates.Green)
						{
							if (ButtonStates.Red)
							{
								if (++Settings.WaitBeep > MAX_WAITBEEP)
									Settings.WaitBeep = 0;
							}
							if (ButtonStates.Green)
							{
								if (--Settings.WaitBeep > MAX_WAITBEEP)
									Settings.WaitBeep = MAX_WAITBEEP;
							}
							DisplayNum(MENU_LINE_WAITBEEP_X, 152, Settings.WaitBeep, 2);
							WaitRed_Repeat();
							WaitGreen_Repeat();
						}
					}
					DrawMenu(OPT_WAITBEEP);
					break;
				case OPT_WAITBEEP_DELAY:
					LCD_DrawChar(MENU_LINE_WAITBEEP_DELAY_X, 248, 'm', 2);
					POINT_COLOR = RED;
					DisplayNum(MENU_LINE_WAITBEEP_DELAY_X, 224, Settings.WaitBeepDelay, 2);
					while (ButtonStates.Blue)
						;
					while(!ButtonStates.Blue)
					{
						while (ButtonStates.Red || ButtonStates.Green)
						{
							if (ButtonStates.Red)
							{
								if (++Settings.WaitBeepDelay > MAX_WAITBEEP_DELAY)
									Settings.WaitBeepDelay = 0;
							}
							if (ButtonStates.Green)
							{
								if (--Settings.WaitBeepDelay > MAX_WAITBEEP_DELAY)
									Settings.WaitBeepDelay = MAX_WAITBEEP_DELAY;
							}
							DisplayNum(MENU_LINE_WAITBEEP_DELAY_X, 224, Settings.WaitBeepDelay, 2);
							WaitRed_Repeat();
							WaitGreen_Repeat();
						}
					}
					DrawMenu(OPT_WAITBEEP_DELAY);
					break;
				case OPT_HOLDTIME:
					POINT_COLOR = RED;
					DisplayNum(MENU_LINE_HOLDTIME_X, 212, Settings.HoldTime, 2);
					while (ButtonStates.Blue)
						;
					while(!ButtonStates.Blue)
					{
						while (ButtonStates.Red || ButtonStates.Green)
						{
							if (ButtonStates.Red)
							{
								if (++Settings.HoldTime > MAX_HOLDTIME)
									Settings.HoldTime = 0;
							}
							if (ButtonStates.Green)
							{
								if (--Settings.HoldTime > MAX_HOLDTIME)
									Settings.HoldTime = MAX_HOLDTIME;
							}
							DisplayNum(MENU_LINE_HOLDTIME_X, 212, Settings.HoldTime, 2);
							WaitRed_Repeat();
							WaitGreen_Repeat();
						}
					}
					DrawMenu(OPT_HOLDTIME);
					break;
				case OPT_ACTIVEBEEP:
					LCD_DrawChar(MENU_LINE_ACTIVEBEEP_X, 200, 's', 2);
					POINT_COLOR = RED;
					DisplayNum(MENU_LINE_ACTIVEBEEP_X, 176, Settings.ActiveBeep, 2);
					while (ButtonStates.Blue)
						;
					while(!ButtonStates.Blue)
					{
						while (ButtonStates.Red || ButtonStates.Green)
						{
							if (ButtonStates.Red)
							{
								if (++Settings.ActiveBeep > MAX_ACTIVEBEEP)
									Settings.ActiveBeep = 0;
							}
							if (ButtonStates.Green)
							{
								if (--Settings.ActiveBeep > MAX_ACTIVEBEEP)
									Settings.ActiveBeep = MAX_ACTIVEBEEP;
							}
							DisplayNum(MENU_LINE_ACTIVEBEEP_X, 176, Settings.ActiveBeep, 2);
							WaitRed_Repeat();
							WaitGreen_Repeat();
						}
					}
					DrawMenu(OPT_ACTIVEBEEP);
					break;
				case OPT_ABEEP_WHILE_PAUSED:
					if (Settings.ABeepWhilePaused)
						Settings.ABeepWhilePaused = 0;
					else
						Settings.ABeepWhilePaused = 1;
					DrawMenu(OPT_ABEEP_WHILE_PAUSED);
					break;
				case OPT_BEEP_DURING_SWITCH:
					if (Settings.BeepDuringSwitch)
						Settings.BeepDuringSwitch = 0;
					else
						Settings.BeepDuringSwitch = 1;
					DrawMenu(OPT_BEEP_DURING_SWITCH);
					break;
				case OPT_BEEP_ON_SWITCH:
					if (Settings.BeepOnSwitch)
						Settings.BeepOnSwitch = 0;
					else
						Settings.BeepOnSwitch = 1;
					DrawMenu(OPT_BEEP_ON_SWITCH);
					break;
				case OPT_RESET_ON_SWITCH:
					if (++Settings.CDSwitchBehavior > 2)
						Settings.CDSwitchBehavior = 0;
					DrawMenu(OPT_RESET_ON_SWITCH);
					break;
				case OPT_START:
					RunTimer();
					LCD_Blank();
					POINT_COLOR = WHITE;
					DrawBorder();
					DrawMenu(0xFF);
					break;
				default:
					break;
			}
			POINT_COLOR = RED;
			DrawCursor(menu_opt, 0);
			while (ButtonStates.Blue)
				;
		}
	}
}

void WaitRed_Repeat()
{
	countdown_ms = MENU_REPEAT_TIME;
	TimerStatus.countdown_ms_go = 1;
	while (ButtonStates.Red && (red_holdtime < 1 || TimerStatus.countdown_ms_go))
		;
}

void WaitGreen_Repeat()
{
	countdown_ms = MENU_REPEAT_TIME;
	TimerStatus.countdown_ms_go = 1;
	while (ButtonStates.Green && (green_holdtime < 1 || TimerStatus.countdown_ms_go))
		;
}

void DrawBorder()
{
	LCD_Fill(235,0,239,319, POINT_COLOR);
	LCD_Fill(4,315,234,319, POINT_COLOR);
	LCD_Fill(0,0,4,319, POINT_COLOR);
	LCD_Fill(4,0,234,4, POINT_COLOR);
}

void DrawMenu(uint8_t lineupdate)
{
	POINT_COLOR = WHITE;
	if (lineupdate == 0xFF)
	{
		LCD_DrawString(198,115, "Setup", 3);
		LCD_Fill(189,30,190,290, RED);
		LCD_DrawString(MENU_LINE_ABEEP_WHILE_PAUSED_X, 20, "Act.beep when paused:", 2);
		LCD_DrawString(MENU_LINE_ACTIVEBEEP_X, 20, "Active beep:   s", 2);
		LCD_DrawString(MENU_LINE_BEEP_DURING_SWITCH_X, 20, "Alert during switch:", 2);
		LCD_DrawString(MENU_LINE_HOLDTIME_X, 20, "Hold to switch:   s", 2);
		LCD_DrawString(MENU_LINE_BEEP_ON_SWITCH_X, 20, "Alert on switch:", 2);
		LCD_DrawString(MENU_LINE_WAITBEEP_DELAY_X, 20, "Wait beep delay:   m", 2);
		LCD_DrawString(MENU_LINE_WAITBEEP_X, 20, "Wait beep:   s", 2);
		LCD_DrawString(MENU_LINE_START_X, 20, "Start", 2);
		LCD_DrawString(10,274, VERSION_STRING, 1);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_TIMER_MODE)
	{
		LCD_DrawString(MENU_LINE_TIMER_MODE_X, 20, TimerStatus.Countdown ? "Countdown: " : "Normal mode          ", 2);
		if (TimerStatus.Countdown)
		{
			DisplayNum(MENU_LINE_TIMER_MODE_X, 152, countdown_h, 2);
			LCD_DrawChar(MENU_LINE_TIMER_MODE_X, 176, ':', 2);
			DisplayNum(MENU_LINE_TIMER_MODE_X, 188, countdown_m, 2);
			LCD_DrawChar(MENU_LINE_TIMER_MODE_X, 212, ':', 2);
			DisplayNum(MENU_LINE_TIMER_MODE_X, 224, countdown_s, 2);
		}
	}
	if (lineupdate == 0xFF || lineupdate == OPT_WAITBEEP)
	{
		if (Settings.WaitBeep)
			DisplayNum(MENU_LINE_WAITBEEP_X, 152, Settings.WaitBeep, 2);
		else
			LCD_DrawString(MENU_LINE_WAITBEEP_X, 152, "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_WAITBEEP_DELAY)
	{
		if (Settings.WaitBeepDelay)
			DisplayNum(MENU_LINE_WAITBEEP_DELAY_X, 224, Settings.WaitBeepDelay, 2);
		else
			LCD_DrawString(MENU_LINE_WAITBEEP_DELAY_X, 224, "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_HOLDTIME)
	{
		DisplayNum(MENU_LINE_HOLDTIME_X, 212, Settings.HoldTime, 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_ACTIVEBEEP)
	{
		if (Settings.ActiveBeep)
			DisplayNum(MENU_LINE_ACTIVEBEEP_X, 176, Settings.ActiveBeep, 2);
		else
			LCD_DrawString(MENU_LINE_ACTIVEBEEP_X, 176, "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_ABEEP_WHILE_PAUSED)
	{
		LCD_DrawString(MENU_LINE_ABEEP_WHILE_PAUSED_X, 272, Settings.ABeepWhilePaused ? "ON " : "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_BEEP_DURING_SWITCH)
	{
		LCD_DrawString(MENU_LINE_BEEP_DURING_SWITCH_X, 272, Settings.BeepDuringSwitch ? "ON " : "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_BEEP_ON_SWITCH)
	{
		LCD_DrawString(MENU_LINE_BEEP_ON_SWITCH_X, 224, Settings.BeepOnSwitch ? "ON " : "OFF", 2);
	}
	if (lineupdate == 0xFF || lineupdate == OPT_RESET_ON_SWITCH)
	{
		if (Settings.CDSwitchBehavior == CD_RESET_ON_SWITCH)
			LCD_DrawString(MENU_LINE_RESET_ON_SWITCH_X, 20, "Reset c/d on switch    ", 2);
		else if (Settings.CDSwitchBehavior == CD_STOP_ON_SWITCH)
			LCD_DrawString(MENU_LINE_RESET_ON_SWITCH_X, 20, "End c/d on switch  ", 2);
		else
			LCD_DrawString(MENU_LINE_RESET_ON_SWITCH_X, 20, "Normal c/d timer switch", 2);
	}
}

void DrawCursor(uint8_t pos, _Bool clear)
{
	uint8_t line = MENU_LINE1_X - (MENU_LINEHEIGHT * pos);
	LCD_DrawChar(line, 8, clear ? ' ' : '>', 2);
}

void DisplayNum(uint8_t x, uint16_t y, uint8_t num, uint8_t size)
{
	char str[3];
	if (num > 99)
	{
		str[0] = '#';
		str[1] = '#';
	}
	else
	{
		str[0] = '0' + num/10;
		str[1] = '0' + num%10;
	}
	str[2] = 0;
	LCD_DrawString(x, y, str, size);
}

void RunTimer()
{
	LCD_Blank();
	red_h = 0;
	red_m = 0;
	red_s = 0;
	green_h = 0;
	green_m = 0;
	green_s = 0;
	red_cd_h = countdown_h;
	red_cd_m = countdown_m;
	red_cd_s = countdown_s;
	green_cd_h = countdown_h;
	green_cd_m = countdown_m;
	green_cd_s = countdown_s;
	TimerStatus.Active = 0;
	TimerStatus.Contested = 0;
	POINT_COLOR = WHITE;
	LCD_DrawString(186,97, "Waiting", 3);
	DrawBorder();
	POINT_COLOR = RED;
	LCD_DrawString(122,102, ":  :", 5);
	POINT_COLOR = GREEN;
	LCD_DrawString(78,102, ":  :", 5);
	TimerStatus.DisplayAlt = 0;
	if (TimerStatus.Countdown)
		TimerStatus.DisplayAlt = 1;
	TimerStatus.UpdateRed = 1;
	TimerStatus.UpdateGreen = 1;
	TimerStatus.Refresh = 1;
	UpdateTimerDisplay();
	while (ButtonStates.Red || ButtonStates.Green || ButtonStates.Blue)
		;
	TimerStatus.countdown1_go = 0;
	if (Settings.WaitBeep)
	{
		countdown1 = Settings.WaitBeepDelay * 60;
		if (!countdown1)
			countdown1 = Settings.WaitBeep;
		TimerStatus.countdown1_go = 1;
	}
	TimerStatus.countdown2_go = 0;
	TimerStatus.countdown_ms_go = 0;
// Wait for timer to be started. Hold blue button for 3s to reset back to menu.
// If the timer has been configured to beep while waiting, countdown1 will be loaded
// with the delay value and it will start beeping once it reaches 0.
// countdown2 will be used for the delay between beeps.
	while (!TimerStatus.Active)
	{
		if (TimerStatus.Contested)
		{
			if (Settings.BeepDuringSwitch && !TimerStatus.countdown_ms_go)
			{
				beep = BEEP_DURING_SWITCH_LENGTH;
				countdown_ms = BEEP_DURING_SWITCH_LENGTH + TIME_BETWEEN_SWITCH_BEEPS;
				TimerStatus.beep = 1;
				TimerStatus.countdown_ms_go = 1;
			}
			if (ButtonStates.Red && !ButtonStates.Green)
			{
				POINT_COLOR = RED;
				if (red_holdtime >= Settings.HoldTime)
				{
					TimerStatus.CurrentTimer = RED_TIMER;
					TimerStatus.Active = 1;
					DrawBorder();
				}
				else if (!TimerStatus.countdown2_go)
				{
					DisplayNum(186,142, Settings.HoldTime - red_holdtime, 3);
					countdown2 = 1;
					TimerStatus.countdown2_go = 1;
				}
			}
			else if (ButtonStates.Green && !ButtonStates.Red)
			{
				POINT_COLOR = GREEN;
				if (green_holdtime >= Settings.HoldTime)
				{
					TimerStatus.CurrentTimer = GREEN_TIMER;
					TimerStatus.Active = 1;
					DrawBorder();
				}
				else if (!TimerStatus.countdown2_go)
				{
					DisplayNum(186,142, Settings.HoldTime - green_holdtime, 3);
					countdown2 = 1;
					TimerStatus.countdown2_go = 1;
				}
			}
			else
			{
				LCD_Fill(186,142,209,178, BLACK);
				POINT_COLOR = WHITE;
				LCD_DrawString(186,97, "Waiting", 3);
				TimerStatus.Contested = 0;
				TimerStatus.countdown2_go = 0;
			}
		}
		else
		{
			if (Settings.WaitBeep && !TimerStatus.countdown1_go)
			{
				beep = IDLE_BEEP_LENGTH;
				countdown1 = Settings.WaitBeep;
				TimerStatus.beep = 1;
				TimerStatus.countdown1_go = 1;
			}
			if ((ButtonStates.Red && !ButtonStates.Green) || (ButtonStates.Green && !ButtonStates.Red))
			{
				TimerStatus.Contested = 1;
				LCD_Fill(186,97,209,223, BLACK);
			}
			else
			{
				red_holdtime = 0;
				green_holdtime = 0;
			}
		}
		if (blue_holdtime > 2)
		{
			break;
		}
	}
// Continue if the timer is now active - otherwise just clean up and return to the menu
// countdown1 is used to time the delay between beeps if the active beep option is enabled
// countdown_ms is used to time the delay between timer switch beeps if that option is enabled
	if (TimerStatus.Active)
	{
		TimerStatus.Contested = 0;
		TimerStatus.countdown1_go = 0;
		TimerStatus.countdown2_go = 0;
		TimerStatus.countdown_ms_go = 0;
		LCD_Fill(186,97,209,223, BLACK); // Clear "Waiting" text
		if (Settings.BeepOnSwitch)
		{
			beep = BEEP_ON_SWITCH_LENGTH;
			TimerStatus.beep = 1;
		}
		if (Settings.ActiveBeep)
		{
			countdown1 = Settings.ActiveBeep;
			TimerStatus.countdown1_go = 1;
		}
		while (1)
		{
			if (TimerStatus.Active)
			{
				UpdateTimerDisplay();
				if (TimerStatus.Countdown)
				{
					if (TimerStatus.CurrentTimer == RED_TIMER)
					{
						if (!red_cd_h && !red_cd_m && !red_cd_s)
							break;
					}
					else if (!green_cd_h && !green_cd_m && !green_cd_s)
						break;
				}
				if (ButtonStates.Red && !ButtonStates.Green && TimerStatus.CurrentTimer == GREEN_TIMER)
				{
					TimerStatus.Active = 0;
					TimerStatus.Contested = 1;
				}
				else if (ButtonStates.Green && !ButtonStates.Red && TimerStatus.CurrentTimer == RED_TIMER)
				{
					TimerStatus.Active = 0;
					TimerStatus.Contested = 1;
				}
				else
				{
					red_holdtime = 0;
					green_holdtime = 0;
				}
				if (ButtonStates.Blue)
				{
					TimerStatus.Active = 0;
					POINT_COLOR = WHITE;
					LCD_DrawString(186,106, "Paused", 3);
					TimerStatus.DisplayAlt = 0;
				}
				if (!TimerStatus.Active)
				{
					TMR0H = TIMER0_RESETVAL >> 8;
					TMR0L = (uint8_t)TIMER0_RESETVAL;
				}
			}
			if (!TimerStatus.Active)
			{
				if (TimerStatus.Contested)
				{
					if (Settings.BeepDuringSwitch && !TimerStatus.countdown_ms_go)
					{
						beep = BEEP_DURING_SWITCH_LENGTH;
						countdown_ms = BEEP_DURING_SWITCH_LENGTH + TIME_BETWEEN_SWITCH_BEEPS;
						TimerStatus.beep = 1;
						TimerStatus.countdown_ms_go = 1;
					}
					if (ButtonStates.Red && !ButtonStates.Green)
					{
						POINT_COLOR = RED;
						if (red_holdtime >= Settings.HoldTime)
						{
							TimerStatus.Contested = 0;
							TimerStatus.CurrentTimer = RED_TIMER;
							TimerStatus.Active = 1;
							if (Settings.BeepOnSwitch)
							{
								beep = BEEP_ON_SWITCH_LENGTH;
								TimerStatus.beep = 1;
							}
							DrawBorder();
							if (TimerStatus.Countdown && Settings.CDSwitchBehavior != 0)
							{
								if (Settings.CDSwitchBehavior == CD_RESET_ON_SWITCH)
								{
									green_cd_h = countdown_h;
									green_cd_m = countdown_m;
									green_cd_s = countdown_s;
									TimerStatus.UpdateGreen = 1;
								}
								else
								{
									red_cd_h = 0;
									red_cd_m = 0;
									red_cd_s = 0;
									TimerStatus.UpdateRed = 1;
								}
								TimerStatus.Refresh = 1;
							}
						}
						else if (!TimerStatus.countdown2_go)
						{
							DisplayNum(186,142, Settings.HoldTime - red_holdtime, 3);
							countdown2 = 1;
							TimerStatus.countdown2_go = 1;
						}
					}
					else if (!ButtonStates.Red && ButtonStates.Green)
					{
						POINT_COLOR = GREEN;
						if (green_holdtime >= Settings.HoldTime)
						{
							TimerStatus.Contested = 0;
							TimerStatus.CurrentTimer = GREEN_TIMER;
							TimerStatus.Active = 1;
							if (Settings.BeepOnSwitch)
							{
								beep = BEEP_ON_SWITCH_LENGTH;
								TimerStatus.beep = 1;
							}
							DrawBorder();
							if (TimerStatus.Countdown && Settings.CDSwitchBehavior != 0)
							{
								if (Settings.CDSwitchBehavior == CD_RESET_ON_SWITCH)
								{
									red_cd_h = countdown_h;
									red_cd_m = countdown_m;
									red_cd_s = countdown_s;
									TimerStatus.UpdateRed = 1;
								}
								else
								{
									green_cd_h = 0;
									green_cd_m = 0;
									green_cd_s = 0;
									TimerStatus.UpdateGreen = 1;
								}
								TimerStatus.Refresh = 1;
							}
						}
						else if (!TimerStatus.countdown2_go)
						{
							DisplayNum(186,142, Settings.HoldTime - green_holdtime, 3);
							countdown2 = 1;
							TimerStatus.countdown2_go = 1;
						}
					}
					else
					{
						TimerStatus.Contested = 0;
						TimerStatus.Active = 1;
					}
					if (!TimerStatus.Contested)
					{
						TimerStatus.countdown2_go = 0;
						LCD_Fill(186,142,209,178, BLACK);
					}
				}
				else
				{
					if (ButtonStates.Blue)
					{
						if (TimerStatus.Countdown)
							ToggleAltDisplay();
						while (ButtonStates.Blue && blue_holdtime < 3)
							;
						if (blue_holdtime > 2)
							break;
					}
					else if (ButtonStates.Red || ButtonStates.Green)
					{
						LCD_Fill(186,106,209,214, BLACK);
						if (TimerStatus.Countdown && !TimerStatus.DisplayAlt)
							ToggleAltDisplay();
						TimerStatus.Active = 1;
					}
				}
			}
			if (Settings.ActiveBeep && (TimerStatus.Active || Settings.ABeepWhilePaused) && !TimerStatus.countdown1_go)
			{
				if (!TimerStatus.beep)
				{
					beep = IDLE_BEEP_LENGTH;
					TimerStatus.beep = 1;
				}
				countdown1 = Settings.ActiveBeep;
				TimerStatus.countdown1_go = 1;
			}
		}
	}
	TimerStatus.Active = 0;
	if (blue_holdtime < 3)
	{
		TimerStatus.Refresh = 1;
		UpdateTimerDisplay();
	}
	if (TimerStatus.Countdown && blue_holdtime < 3)
	{
		POINT_COLOR = WHITE;
		LCD_DrawString(186,88, "Finished", 3);
		countdown1 = COUNTDOWN_ALERT_DURATION;
		TimerStatus.countdown1_go = 1;
		_Bool AllButtonsReleased = 0;
		uint8_t beepcount = COUNTDOWN_ALERT_NUM_BEEPS;
		TimerStatus.countdown_ms_go = 0;
		while (blue_holdtime < 3)
		{
			if (!ButtonStates.Red && !ButtonStates.Green && !ButtonStates.Blue)
				AllButtonsReleased = 1;
			if (AllButtonsReleased && (ButtonStates.Red || ButtonStates.Green || ButtonStates.Blue))
			{
				TimerStatus.countdown1_go = 0;
				if (ButtonStates.Blue)
				{
					ToggleAltDisplay();
					while (ButtonStates.Blue && blue_holdtime < 3)
						;
				}
			}
			if (TimerStatus.countdown1_go && !TimerStatus.countdown_ms_go)
			{
				beep = COUNTDOWN_ALERT_BEEP_LENGTH;
				if (--beepcount == 0)
				{
					beepcount = COUNTDOWN_ALERT_NUM_BEEPS;
					countdown_ms = COUNTDOWN_ALERT_BEEP_LENGTH + COUNTDOWN_ALERT_PAUSE;
				}
				else
					countdown_ms = COUNTDOWN_ALERT_BEEP_LENGTH + COUNTDOWN_ALERT_BEEP_DELAY;
				TimerStatus.beep = 1;
				TimerStatus.countdown_ms_go = 1;
			}
		}
	}
}

// Only the seconds counters are modified by the Timer0 interrupt; the minute and hour variables
// are updated here.
// To help reduce power consumption only the fields that get updated are redrawn, unless
// TimerStatus.Refresh is set (will be cleared automatically prior to returning)

void UpdateTimerDisplay()
{
	struct
	{
		_Bool Minute : 1;
		_Bool Hour : 1;
	} Update;
	if (TimerStatus.UpdateRed)
	{
		Update.Minute = 0;
		Update.Hour = 0;
		if (red_s > 59)
		{
			red_s -= 60;
			red_m++;
			if (!TimerStatus.Countdown)
				Update.Minute = 1;
		}
		if (red_m > 59)
		{
			red_m -= 60;
			red_h++;
			if (!TimerStatus.Countdown)
				Update.Hour = 1;
		}
		if (red_h > 99)
			red_h -= 100;
		if (TimerStatus.Countdown)
		{
			if (red_cd_s > 59)
			{
				red_cd_s += 60;
				red_cd_m--;
				Update.Minute = 1;
			}
			if (red_cd_m > 59)
			{
				red_cd_m += 60;
				red_cd_h--;
				Update.Hour = 1;
			}
			if (red_cd_h > 99)
				red_cd_h += 100;
		}
		POINT_COLOR = RED;
		DisplayNum(122,220, TimerStatus.DisplayAlt ? red_cd_s : red_s, 5);
		if (Update.Minute || TimerStatus.Refresh)
			DisplayNum(122,130, TimerStatus.DisplayAlt ? red_cd_m : red_m, 5);
		if (Update.Hour || TimerStatus.Refresh)
			DisplayNum(122,40, TimerStatus.DisplayAlt ? red_cd_h : red_h, 5);
		TimerStatus.UpdateRed = 0;
	}
	if (TimerStatus.UpdateGreen)
	{
		Update.Minute = 0;
		Update.Hour = 0;
		if (green_s > 59)
		{
			green_s -= 60;
			green_m++;
			if (!TimerStatus.Countdown)
				Update.Minute = 1;
		}
		if (green_m > 59)
		{
			green_m -= 60;
			green_h++;
			if (!TimerStatus.Countdown)
				Update.Hour = 1;
		}
		if (green_h > 99)
			green_h -= 100;
		if (TimerStatus.Countdown)
		{
			if (green_cd_s > 59)
			{
				green_cd_s += 60;
				green_cd_m--;
				Update.Minute = 1;
			}
			if (green_cd_m > 59)
			{
				green_cd_m += 60;
				green_cd_h--;
				Update.Hour = 1;
			}
			if (green_cd_h > 99)
				green_cd_h += 100;
		}
		POINT_COLOR = GREEN;
		DisplayNum(78,220, TimerStatus.DisplayAlt ? green_cd_s : green_s, 5);
		if (Update.Minute || TimerStatus.Refresh)
			DisplayNum(78,130, TimerStatus.DisplayAlt ? green_cd_m : green_m, 5);
		if (Update.Hour || TimerStatus.Refresh)
			DisplayNum(78,40, TimerStatus.DisplayAlt ? green_cd_h : green_h, 5);
		TimerStatus.UpdateGreen = 0;
	}
	TimerStatus.Refresh = 0;
}

void ToggleAltDisplay()
{
	POINT_COLOR = WHITE;
	if (TimerStatus.DisplayAlt)
		TimerStatus.DisplayAlt = 0;
	else
		TimerStatus.DisplayAlt = 1;
	LCD_DrawString(40,57, TimerStatus.DisplayAlt ? "            " : "Elapsed time", 3);
	TimerStatus.Refresh = 1;
	TimerStatus.UpdateRed = 1;
	TimerStatus.UpdateGreen = 1;
	UpdateTimerDisplay();
}
