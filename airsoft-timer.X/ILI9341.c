/*
 *
 *	PIC18-ILI9341
 *
 *	Library for using ILI9341-based 240x320 TFT displays with PIC microcontrollers. This is comprised of 3 files:
 *	ILI9341.c, ILI9341.h and ILI9341-Graphic-Font.h
 *
 *	Various resources were used as references:
 *
 *	- The ILI9341 datasheet (of course)
 *	- Project sources/examples from Practical Electronics magazine ( https://www.electronpublishing.com/ )
 *	- Examples from https://www.lcdwiki.com/2.2inch_SPI_Module_ILI9341_SKU:MSP2202 and elsewhere
 *	- Info found in discussions online
 *
 *	This is geared towards fixed X=240 Y=320 and values for X use 8-bit ints to reduce code size and improve speed.
 *
 *	Remember to set definitions for ILI9341_RESET, ILI9341_DC and ILI9341_CS as appropriate for your project; these can be found in ILI9341.h.
 *	_XTAL_FREQ will also need to be defined for the delays in the init function.
 *
 *	Usage:
 *
 *	Call LCD_Init() to set up the display, then set BACK_COLOR to whatever colour you want to fill the display with (default is BLACK)
 *	and then call either LCD_Clear() or LCD_Blank() to fill the display with the color value set in BACK_COLOR. The display is now ready.
 *
 *	Use the various functions here to write to the display; with the exception of LCD_Fill() they will use the color value set in POINT_COLOR
 *	If you used LCD_Blank() then POINT_COLOR will be set to BACK_COLOR and need changing; otherwise the default value for POINT_COLOR is WHITE
 *	For the LCD_DrawString() function, LINE_START defines the value to reset the Y axis to for newlines (default is 0).
 *
 *	Note that stuff like boundary checking generally isn't performed for size + speed reasons; generally it's not too hard to avoid it being an issue
 *	but if you really need it you can add it yourself in your main program or by modifying the routines here.
 *
 *	--------------------
 *
 *	This has been slightly modified to better suit the airsoft timer's hardware:
 *
 *	- As the LCD is the only SPI device connected, its CS pin has been hard-wired and does not
 *		need to be toggled by the SPI write functions.
 *
 *	- Also the checks for the current SPI port status are front-loaded so that processing of
 *		further data/continuing with the rest of the program will not be held up by the current
 *		SPI transfer and everything can be done a bit faster.
 *
 *	- The PIC18FxxK42 MCUs feature SPI ports with 2-byte FIFO buffers, so there's no need to wait
 *		for the first byte of a 16-bit transfer to send before writing the second byte.
 *
 *	- As there is no need to read anything from the display SPI RX is disabled so there is no
 *		need to clear the RX buffers after transferring data to the display.
 *
 */

#include <xc.h>
#include <stdlib.h>
#include <stdint.h>
#include <pic18f24k42.h>
#include "ILI9341.h"
#include "ILI9341-Graphic-Font.h"
#include "airsoft-timer.h"

uint16_t BACK_COLOR, POINT_COLOR, LINE_START;

void LCD_WriteByte(uint8_t data)
{
	while (!SPI1STATUSbits.TXBE)
		;
	ILI9341_DC = 1;
	SPI1TXB = data;
}

void LCD_Write16b(uint16_t data)
 {
	while (!SPI1STATUSbits.TXBE)
		;
	ILI9341_DC = 1;
	SPI1TXB = data>>8;
	SPI1TXB = (uint8_t) data;
}

void LCD_CMD(uint8_t cmd)
{
	while (!SPI1STATUSbits.TXBE)
		;
	ILI9341_DC = 0;
	SPI1TXB = cmd;
}

void LCD_SetDrawArea(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2)
{
	LCD_CMD(ILI9341_CASET);
	LCD_Write16b(x1);
	LCD_Write16b(x2);

	LCD_CMD(ILI9341_PASET);
	LCD_Write16b(y1);
	LCD_Write16b(y2);

	LCD_CMD(ILI9341_WR_RAM);
}

void LCD_Clear()
{
	LCD_SetDrawArea(0,0, LCD_W-1, LCD_H-1);
	for (uint8_t x = 0; x < LCD_W; x++)
		for (uint16_t y = 0; y < LCD_H; y++)
			LCD_Write16b(BACK_COLOR);
}

// Clearing the whole LCD takes a while due to its SPI interface.
// This routine is intended to disguise this, drawing from the center to the outer edges and making it look
// more like a deliberate transition effect. Make sure to reset POINT_COLOR to whatever you want afterwards.

void LCD_Blank()
{
	POINT_COLOR = BACK_COLOR;
	for (uint8_t x = 120; (int8_t) x >= 0; x--)
		LCD_DrawRectangle(x, x, 239-x, (uint16_t) 319-x);
}

void LCD_DrawPoint(uint8_t x, uint16_t y)
{
	LCD_SetDrawArea(x, y, x, y);
	LCD_Write16b(POINT_COLOR);
}

void LCD_Fill(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2, uint16_t color)
{
	LCD_SetDrawArea(x1, y1, x2, y2);
	for (uint8_t x = x1; x <= x2; x++)
		for (uint16_t y = y1; y <= y2; y++)
			LCD_Write16b(color);
}

void LCD_DrawPoint_big(uint8_t x, uint16_t y)
{
	LCD_Fill(x-1, y-1, x+1, y+1, POINT_COLOR);
}

void LCD_DrawLine(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2)
{
	uint8_t Xpos = x1;
	uint16_t Ypos = y1;
	int8_t DirX, DirY;
	int16_t Xadj = 0, Yadj = 0, DeltaX, DeltaY, MaxDelta;

	DeltaX = x2 - x1;
	DeltaY = (int16_t) (y2 - y1);

	if (DeltaX > 0)
		DirX = 1;
	else if (DeltaX == 0)
		DirX = 0;
	else
	{
		DirX = -1;
		DeltaX = -DeltaX;
	}

	if (DeltaY > 0)
		DirY = 1;
	else if (DeltaY == 0)
		DirY = 0;
	else
	{
		DirY = -1;
		DeltaY = -DeltaY;
	}

	if (DeltaX > DeltaY)
		MaxDelta = DeltaX;
	else
		MaxDelta = DeltaY;

	LCD_SetDrawArea(Xpos, Ypos, Xpos, Ypos);
	while (1)
	{
		LCD_Write16b(POINT_COLOR);
		if (Xpos == x2 && Ypos == y2)
			break;
		Xadj += DeltaX;
		Yadj += DeltaY;
		if (Xadj > MaxDelta)
		{
			Xadj -= MaxDelta;
			Xpos = (uint8_t) ((int8_t) Xpos + DirX);
			LCD_CMD(ILI9341_CASET);
			LCD_Write16b(Xpos);
			LCD_Write16b(Xpos);
		}
		if (Yadj > MaxDelta)
		{
			Yadj -= MaxDelta;
			Ypos = (uint16_t) ((int16_t) Ypos + DirY);
			LCD_CMD(ILI9341_PASET);
			LCD_Write16b(Ypos);
			LCD_Write16b(Ypos);
		}
		LCD_CMD(ILI9341_WR_RAM);
	}
}

// Just make sure that x2 >= x1 and y2 >= y1
void LCD_DrawRectangle(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2)
{
	uint16_t tmp;
	LCD_SetDrawArea(x1, y1, x2, y1);
	for (tmp = x1; tmp <= x2; tmp++)
		LCD_Write16b(POINT_COLOR);
	LCD_SetDrawArea(x1, y1, x1, y2);
	for (tmp = y1; tmp <= y2; tmp++)
		LCD_Write16b(POINT_COLOR);
	LCD_SetDrawArea(x1, y2, x2, y2);
	for (tmp = x1; tmp <= x2; tmp++)
		LCD_Write16b(POINT_COLOR);
	LCD_SetDrawArea(x2, y1, x2, y2);
	for (tmp = y1; tmp <= y2; tmp++)
		LCD_Write16b(POINT_COLOR);
}

void LCD_DrawTriangle(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2, uint8_t x3, uint16_t y3)
{
	LCD_DrawLine(x1,y1,x2,y2);
	LCD_DrawLine(x2,y2,x3,y3);
	LCD_DrawLine(x3,y3,x1,y1);
}

void LCD_DrawCircle(uint8_t x, uint16_t y, uint8_t r)
{
	uint8_t a, b;
	int di;

	a = 0;
	b = r;
	di = 3 - (r << 1);

	while (a <= b)
	{
		LCD_DrawPoint(x-b, y-a);
		LCD_DrawPoint(x+b, y-a);
		LCD_DrawPoint(x-a, y+b);
		LCD_DrawPoint(x-a, y-b);
		LCD_DrawPoint(x+b, y+a);
		LCD_DrawPoint(x+a, y-b);
		LCD_DrawPoint(x+a, y+b);
		LCD_DrawPoint(x-b, y+a);
		a++;
		if (di < 0)
			di += 4*a + 6;
		else
		{
			di += 10 + 4*(a-b);
			b--;
		}
	}
}

void LCD_DrawChar(uint8_t x, uint16_t y, uint8_t num, uint8_t size)
{
	uint8_t data;

	if (!size)
		++size;

	num -= ' ';

	LCD_SetDrawArea(x, y, x+8*size-1, y+5*size-1);
	for(uint8_t col = 0; col < 5; col++)
	{
		for(uint8_t c1 = 0; c1 < size; c1++)
		{
			data = Font5x7[num*5 + col];
			for (uint8_t row = 0; row < 8; row++)
			{
				for (uint8_t c2 = 0; c2 < size; c2++)
				{
					if (data & 0x80)
						LCD_Write16b(POINT_COLOR);
					else
						LCD_Write16b(BACK_COLOR);
				}
				data <<= 1;
			}
		}
	}
}

void LCD_DrawString(uint8_t x, uint16_t y, char *p, uint8_t size)
{
	uint8_t charwidth, charheight;
	if (!size)
		++size;

	charwidth = 6 * size;
	charheight = 8 * size;

	while(*p != '\0')
	{
		if (x + charheight > LCD_W)
		{
			x = 239 - charheight;
			y = LINE_START;
		}
		if (y + charwidth > LCD_H || *p == '\n')
		{
			y = LINE_START;
			x -= charheight;
		}
		if (*p != '\n')
		{
			LCD_DrawChar(x, y, *p, size);
			y += charwidth;
		}
		p++;
	}
}

// Lots of mystery commands were present in the various examples I found, with the data components varying between them.
// I couldn't find any explanation for what they do, these commands are not listed in the datasheet,
// and the modules I have appear to work just fine without them.
// So I've not included them for now; can add them later if they actually turn out to do something useful.
// Maybe they're only necessary for older versions of the ILI9341?

void LCD_Init(void)
{
	BACK_COLOR = BLACK;
	POINT_COLOR = WHITE;
	LINE_START = 0;

	ILI9341_RESET = 1;
	__delay_ms(5);
	ILI9341_RESET = 0;
	__delay_ms(1);
	ILI9341_RESET = 1;
	__delay_ms(5);

	LCD_CMD(ILI9341_PIX_FMT);	// Set pixel format
	LCD_WriteByte(0x55);		// 0x55 = 16 bits/pixel for RGB+MCU interfaces (default = 0x66 = 18 bits/pixel)

	LCD_CMD(ILI9341_MEM_ACC_CTL);	// Memory Access Control
	LCD_WriteByte(0xC8);			// 0xC8 = BGR color filter panel, touch screen/SD pins on the right; 0x08 = touch screen/SD pins on the left (flip display 180 degrees)

	LCD_CMD(ILI9341_D_FNC_CTL);	// Display Function Control
	LCD_WriteByte(0x0A);
	LCD_WriteByte(0xA2);

	__delay_ms(115);			// Must wait at least 120ms after releasing RESX before sending the SLPOUT command (datasheet p.230)...
	LCD_CMD(ILI9341_SLPOUT);
	__delay_ms(5);				// ... and 5ms after sending SLPOUT before sending other commands (p.101).
	LCD_CMD(ILI9341_DISP_ON);
}