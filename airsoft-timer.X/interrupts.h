/*
 *	Airsoft Game Timer/Objective
 *	Copyright (C) 2021 Adamhm
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C"
{
#endif

// Timer0 interrupt routine is to run every 1s
#define TIMER0_CLOCK_FREQ		32768
#define TIMER0_RESETVAL			(65535 - TIMER0_CLOCK_FREQ)

// Timer2 interrupt routine is to run roughly every 1ms
#define TIMER2_CLOCK_FREQ		31000
#define TIMER2_PERIOD			TIMER2_CLOCK_FREQ / 1000 - 1

// Button debounce counter / limit before a button is accepted as pressed
#define BUTTON_DEBOUNCE_COUNT	10

struct BUTTON_STATE_STRUCT
{
	_Bool Red : 1;
	_Bool Green : 1;
	_Bool Blue : 1;
};

extern struct BUTTON_STATE_STRUCT ButtonStates;

extern uint8_t red_holdtime;
extern uint8_t green_holdtime;
extern uint8_t blue_holdtime;

extern uint16_t beep;

extern uint16_t countdown1;
extern uint16_t countdown2;
extern uint16_t countdown_ms;

#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */

