/*
 *	Airsoft Game Timer/Objective
 *	Copyright (C) 2021 Adamhm
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <xc.h>
#include <stdlib.h>
#include <stdint.h>
#include <pic18f24k42.h>
#include "interrupts.h"
#include "airsoft-timer.h"

struct BUTTON_STATE_STRUCT ButtonStates;

uint8_t red_holdtime = 0;
uint8_t green_holdtime = 0;
uint8_t blue_holdtime = 0;

uint16_t beep = 0;

uint16_t countdown1 = 0;
uint16_t countdown2 = 0;
uint16_t countdown_ms = 0;

// The Timer0 interrupt runs every 1s (driven by the 32768Hz crystal SOSC) and handles
// high priority/high accuracy tasks such as ticks of the main timer.
// It also keeps track of how long buttons are held down for and runs two countdown timers.
//
// To use the timers, set countdown# and then set TimerStatus.countdown#_go = 1 to activate them.
// TimerStatus.countdown#_go will be cleared when they have expired.
// Only read/write the counters when their TimerStatus bits are clear.
//
// To maintain accurate timing where it matters, Timer0 is reset by the Timer2 interrupt whenever any
// buttons are pressed and the timer is inactive (allowing for better accuracy of button hold times)
// and by the main code whenever the timer is stopped/started.

void __interrupt(irq(TMR0),high_priority) Timer0InterruptRoutine()
{
	TMR0H = TIMER0_RESETVAL >> 8;
	TMR0L = (uint8_t)TIMER0_RESETVAL;
	if (TimerStatus.Active)
	{
		if (TimerStatus.CurrentTimer == GREEN_TIMER)
		{
			++green_s;
			--green_cd_s;
			TimerStatus.UpdateGreen = 1;
		}
		else
		{
			++red_s;
			--red_cd_s;
			TimerStatus.UpdateRed = 1;
		}
	}
	if (ButtonStates.Red && red_holdtime < 255)
		++red_holdtime;

	if (ButtonStates.Green && green_holdtime < 255)
		++green_holdtime;

	if (ButtonStates.Blue && blue_holdtime < 255)
		++blue_holdtime;

	if (TimerStatus.countdown1_go)
		if (--countdown1 == 0)
			TimerStatus.countdown1_go = 0;

	if (TimerStatus.countdown2_go)
		if (--countdown2 == 0)
			TimerStatus.countdown2_go = 0;

	PIR3bits.TMR0IF = 0;
}

// The Timer2 interrupt runs roughly every 1ms (driven by the 31kHz LFINTOSC) and handles low priority/lower
// accuracy timing tasks such as button debouncing, buzzer activation/deactivation and timing, and another
// countdown timer.
//
// Button debouncing is performed using a counter. When a button is pressed, its corresponding counter is
// incremented, and when it is released the counter is decremented. When the counter reaches the upper limit
// defined by BUTTON_DEBOUNCE_COUNT the button is accepted as pressed. When the counter reaches zero, the
// button is accepted as released.
//
// The countdown_ms timer works the same way as countdown1 and countdown2, except it ticks every ~1ms instead
// of 1s. The buzzer also works the same way: set beep = <time>, where <time> is the time in ms that you want the
// buzzer to sound for, and then set TimerStatus.beep = 1.
//
// Whenever a button is pressed while the timer is not active, Timer0 is reset to allow for more accurate counting
// of button hold times.

void __interrupt(irq(TMR2),low_priority) Timer2InterruptRoutine()
{
	static uint8_t RBCounter = 0;
	static uint8_t GBCounter = 0;
	static uint8_t BBCounter = 0;

	if (!RED_BUTTON)
	{
		if (RBCounter < BUTTON_DEBOUNCE_COUNT && ++RBCounter == BUTTON_DEBOUNCE_COUNT)
		{
			if (!TimerStatus.Active)
			{
				TMR0H = TIMER0_RESETVAL >> 8;
				TMR0L = (uint8_t)TIMER0_RESETVAL;
			}
			ButtonStates.Red = 1;
		}
	}
	else if (RBCounter > 0 && --RBCounter == 0)
	{
			ButtonStates.Red = 0;
			red_holdtime = 0;
	}

	if (!GREEN_BUTTON)
	{
		if (GBCounter < BUTTON_DEBOUNCE_COUNT && ++GBCounter == BUTTON_DEBOUNCE_COUNT)
		{
			if (!TimerStatus.Active)
			{
				TMR0H = TIMER0_RESETVAL >> 8;
				TMR0L = (uint8_t)TIMER0_RESETVAL;
			}
			ButtonStates.Green = 1;
		}
	}
	else if (GBCounter > 0 && --GBCounter == 0)
	{
			ButtonStates.Green = 0;
			green_holdtime = 0;
	}

	if (!BLUE_BUTTON)
	{
		if (BBCounter < BUTTON_DEBOUNCE_COUNT && ++BBCounter == BUTTON_DEBOUNCE_COUNT)
		{
			if (!TimerStatus.Active)
			{
				TMR0H = TIMER0_RESETVAL >> 8;
				TMR0L = (uint8_t)TIMER0_RESETVAL;
			}
			ButtonStates.Blue = 1;
		}
	}
	else if (BBCounter > 0 && --BBCounter == 0)
	{
			ButtonStates.Blue = 0;
			blue_holdtime = 0;
	}

	if (TimerStatus.countdown_ms_go)
		if (--countdown_ms == 0)
			TimerStatus.countdown_ms_go = 0;

	if (TimerStatus.beep)
	{
		if (!BUZZER)
			BUZZER = 1;
		else if (--beep == 0)
		{
			TimerStatus.beep = 0;
			BUZZER = 0;
		}
	}

	PIR4bits.TMR2IF = 0;
}
