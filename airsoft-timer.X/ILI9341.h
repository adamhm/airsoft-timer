#ifndef ILI9341_H
#define	ILI9341_H

#ifdef	__cplusplus
extern "C" {
#endif

#define ILI9341_RESET LATCbits.LC4
#define ILI9341_DC LATCbits.LC5
// #define ILI9341_CS

#define WHITE		0xFFFF
#define BLACK		0x0000
#define BLUE		0x001F
#define BRED		0XF81F
#define GRED		0XFFE0
#define GBLUE		0X07FF
#define RED			0xF800
#define MAGENTA		0xF81F
#define GREEN		0x07E0
#define CYAN		0x7FFF
#define YELLOW		0xFFE0
#define BROWN		0XBC40
#define BRRED		0XFC07
#define GRAY		0X8430
#define DARKBLUE	0X01CF
#define LIGHTBLUE	0X7D7C
#define GRAYBLUE	0X5458
#define LIGHTGREEN  0X841F
#define LGRAY		0XC618
#define LGRAYBLUE	0XA651
#define LBBLUE		0X2B12

#define LCD_W 240
#define LCD_H 320

//	ILI9341 commands:

#define ILI9341_NOP			0x00
#define ILI9341_SOFT_RESET	0x01
#define ILI9341_RD_ID		0x04
#define ILI9341_RD_STATUS	0x09
#define ILI9341_RD_PWRMODE	0x0A
#define ILI9341_RD_MADCTL	0x0B
#define ILI9341_RD_PIXFMT	0x0C
#define ILI9341_RD_IMGFMT	0x0D
#define ILI9341_RD_SIGMODE	0x0E
#define ILI9341_RD_SELFDIAG	0x0F

#define ILI9341_SLPIN		0x10
#define ILI9341_SLPOUT		0x11
#define ILI9341_PTL_MODE	0x12
#define ILI9341_NORM_MODE	0x13

#define ILI9341_INVERT_OFF	0x20
#define ILI9341_INVERT_ON	0x21
#define ILI9341_GAMMA_SET	0x26
#define ILI9341_DISP_OFF	0x28
#define ILI9341_DISP_ON		0x29
#define ILI9341_CASET		0x2A
#define ILI9341_PASET		0x2B
#define ILI9341_WR_RAM		0x2C
#define ILI9341_COLOR_SET	0x2D
#define ILI9341_RD_RAM		0x2E

#define ILI9341_PTL_AREA	0x30
#define ILI9341_VSCRL_DEF	0x33
#define ILI9341_TEAR_EFF_OFF	0x34
#define ILI9341_TEAR_EFF_ON	0x35
#define ILI9341_MEM_ACC_CTL	0x36
#define ILI9341_VSCRL_SADDR	0x37
#define ILI9341_IDLE_OFF	0x38
#define ILI9341_IDLE_ON		0x39
#define ILI9341_PIX_FMT		0x3A
#define ILI9341_WR_MEM_CONT	0x3C
#define ILI9341_RD_MEM_CONT	0x3E

#define ILI9341_SET_TEAR_SCNLN	0x44
#define ILI9341_GET_SCNLN	0x45

#define ILI9341_WR_BRIGHTNESS	0x51
#define ILI9341_RD_BRIGHTNESS	0x52
#define ILI9341_WR_CTRL_DISP	0x53
#define ILI9341_RD_CTRL_DISP	0x54
#define ILI9341_WR_ADAPT_BR	0x55
#define ILI9341_RD_ADAPT_BR	0x56
#define ILI9341_WR_ADAPT_BR_MIN	0x5E
#define ILI9341_RD_ADAPT_BR_MIN	0x5F

#define ILI9341_RGB_INT_SIG_CTL	0xB0
#define ILI9341_FRMR_CTL1	0xB1
#define ILI9341_FRMR_CTL2	0xB2
#define ILI9341_FRMR_CTL3	0xB3
#define ILI9341_INV_CTL		0xB4
#define ILI9341_BLANK_P_CTL	0xB5
#define ILI9341_D_FNC_CTL	0xB6
#define ILI9341_ENTRY_MODE_SET	0xB7
#define ILI9341_BL_CTL1		0xB8
#define ILI9341_BL_CTL2		0xB9
#define ILI9341_BL_CTL3		0xBA
#define ILI9341_BL_CTL4		0xBB
#define ILI9341_BL_CTL5		0xBC
#define ILI9341_BL_CTL7		0xBE
#define ILI9341_BL_CTL8		0xBF

#define ILI9341_PWRCTL1		0xC0
#define ILI9341_PWRCTL2		0xC1
#define ILI9341_PWRCTL3		0xC2
#define ILI9341_PWRCTL4		0xC3
#define ILI9341_PWRCTL5		0xC4
#define ILI9341_VCOMCTL1	0xC5
#define ILI9341_VCOMCTL2	0xC7

#define ILI9341_NVM_WR		0xD0
#define ILI9341_NVM_PRT_KEY	0xD1
#define ILI9341_NVM_STATUS	0xD2

#define ILI9341_RD_ID1		0xDA
#define ILI9341_RD_ID2		0xDB
#define ILI9341_RD_ID3		0xDC
#define ILI9341_RD_ID4		0xD3

#define ILI9341_POS_GAM_CORRECT	0xE0
#define ILI9341_NEG_GAM_CORRECT	0xE1
#define ILI9341_DGAMMA_CTL1	0xE2
#define ILI9341_DGAMMA_CTL2	0xE3

#define ILI9341_INTFACE_CTL	0xF6

extern unsigned short BACK_COLOR, POINT_COLOR, LINE_START;

void LCD_WriteByte(uint8_t data);
void LCD_Write16b(uint16_t data);
void LCD_CMD(uint8_t cmd);
void LCD_SetDrawArea(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2);
void LCD_Clear(void);
void LCD_Blank(void);
void LCD_DrawPoint(uint8_t x, uint16_t y);
void LCD_Fill(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2, uint16_t color);
void LCD_DrawPoint_big(uint8_t x, uint16_t y);
void LCD_DrawLine(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2);
void LCD_DrawRectangle(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2);
void LCD_DrawTriangle(uint8_t x1, uint16_t y1, uint8_t x2, uint16_t y2, uint8_t x3, uint16_t y3);
void LCD_DrawCircle(uint8_t x, uint16_t y, uint8_t r);
void LCD_DrawChar(uint8_t x, uint16_t y, uint8_t num, uint8_t size);
void LCD_DrawString(uint8_t x, uint16_t y, char *p, uint8_t size);
void LCD_Init(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ILI9341_H */