/*
 *	Airsoft Game Timer/Objective
 *	Copyright (C) 2021 Adamhm
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef AIRSOFT_TIMER_H
#define	AIRSOFT_TIMER_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define _XTAL_FREQ 64000000

#define RED_BUTTON		PORTAbits.RA0
#define GREEN_BUTTON	PORTAbits.RA1
#define BLUE_BUTTON		PORTAbits.RA2

#define BUZZER			PWM5CONbits.EN
#define LCD_BACKLIGHT	PWM6CONbits.EN

#define VERSION_STRING "v1.00"

// Limits in s except for MAX_WAITBEEP_DELAY which is in minutes

#define MAX_WAITBEEP		60
#define MAX_WAITBEEP_DELAY	30
#define MAX_HOLDTIME		60
#define MAX_ACTIVEBEEP		60

// Idle/active/switch beep timings, all in ms

#define IDLE_BEEP_LENGTH			300
#define BEEP_ON_SWITCH_LENGTH		1000
#define BEEP_DURING_SWITCH_LENGTH	100
#define TIME_BETWEEN_SWITCH_BEEPS	50

// Countdown alert beep settings, aside from duration all timings are in ms

#define COUNTDOWN_ALERT_DURATION	30
#define COUNTDOWN_ALERT_NUM_BEEPS	3
#define COUNTDOWN_ALERT_BEEP_LENGTH	300
#define COUNTDOWN_ALERT_BEEP_DELAY	100
#define COUNTDOWN_ALERT_PAUSE		1000

#define RED_TIMER		0
#define GREEN_TIMER		1

#define CD_RESET_ON_SWITCH	1
#define CD_STOP_ON_SWITCH	2

#define MENU_LINEHEIGHT	17
#define MENU_LINE1_X	163

enum
{
	OPT_TIMER_MODE,
	OPT_WAITBEEP,
	OPT_WAITBEEP_DELAY,
	OPT_HOLDTIME,
	OPT_ACTIVEBEEP,
	OPT_ABEEP_WHILE_PAUSED,
	OPT_BEEP_DURING_SWITCH,
	OPT_BEEP_ON_SWITCH,
	OPT_RESET_ON_SWITCH,
	OPT_START,
	NUM_MENU_LINES
};

#define	MENU_LINE_WAITBEEP_X			MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_WAITBEEP)
#define	MENU_LINE_WAITBEEP_DELAY_X		MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_WAITBEEP_DELAY)
#define	MENU_LINE_HOLDTIME_X			MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_HOLDTIME)
#define	MENU_LINE_ACTIVEBEEP_X			MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_ACTIVEBEEP)
#define	MENU_LINE_ABEEP_WHILE_PAUSED_X	MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_ABEEP_WHILE_PAUSED)
#define	MENU_LINE_BEEP_DURING_SWITCH_X	MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_BEEP_DURING_SWITCH)
#define	MENU_LINE_BEEP_ON_SWITCH_X		MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_BEEP_ON_SWITCH)
#define	MENU_LINE_TIMER_MODE_X			MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_TIMER_MODE)
#define	MENU_LINE_RESET_ON_SWITCH_X		MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_RESET_ON_SWITCH)
#define	MENU_LINE_START_X				MENU_LINE1_X - (MENU_LINEHEIGHT * OPT_START)

#define MENU_REPEAT_TIME	75

struct TIMER_STATUS_STRUCT
{
	_Bool Countdown : 1;
	_Bool Active : 1;
	_Bool CurrentTimer : 1;
	_Bool Contested : 1;
	_Bool UpdateRed : 1;
	_Bool UpdateGreen : 1;
	_Bool Refresh : 1;
	_Bool DisplayAlt : 1;
	_Bool countdown1_go : 1;
	_Bool countdown2_go : 1;
	_Bool countdown_ms_go : 1;
	_Bool beep : 1;
};

extern struct TIMER_STATUS_STRUCT TimerStatus;

extern uint8_t red_s;
extern uint8_t red_cd_s;
extern uint8_t green_s;
extern uint8_t green_cd_s;

#ifdef	__cplusplus
}
#endif

#endif	/* AIRSOFT_TIMER_H */